# -*- coding: utf-8 -*-
"""
Author: Germán Santa-Maria
Script: Process Grades

Description: 
    This script will take a filled PDF forms as input, and will produce
    a CSV file with the values extracted from the information contained 
    in the forms
 
Conditions: 
    All the pdfs should come from the same form template (same number of 
    fillable text boxes).
"""

"""-------------------------Hard coded input--------------------------------"""
# Input file path
pdfs_path = 'script_input/pdfs/' # pdf files path 

# Output file path
csv_file_name = 'script_output/grades.csv' # csv file name (to store grades)
"""-------------------------------------------------------------------------"""

# Import modules
import os
import PyPDF2 # For working with PDFs
import csv # For reading cvs files

# List of all the files in the directory
list_of_files = os.listdir(pdfs_path)

# Filter only ".pdf" files
list_of_pdf_files = []
for i in range(len(list_of_files)) :
    if list_of_files[i].endswith(".pdf"):
        list_of_pdf_files.append(list_of_files[i])

if len(list_of_pdf_files) == 0:
    raise Exception("No pdf files were found in the selected path")
    
#---------------Create the headers for the output csv file--------------------#
# Select first pdf in the list as calibration pdf
calibration_pdf = list_of_pdf_files[0]
    
# Read PDF file
object = open(pdfs_path + calibration_pdf, 'rb') # rb means read binary
reader = PyPDF2.PdfFileReader(object)

# Get values from text boxes in the PDF
#text_boxes_data = reader.getFormTextFields() # Deprecated, now getFields() works
text_boxes_data = reader.getFields()

# Create headers for the csv file
headers = []
headers.append('pdf_name')
text_boxes_names = list(text_boxes_data.keys())
for i in range(len(text_boxes_names)):
    headers.append(str(text_boxes_names[i]))

#------------------Extract grades from all the pdf files----------------------#
line_data = [] # Array with the data to be printed in each line of the CSV file
errors =    [] # List of pdfs that could not be processed
for pdf_file in list_of_pdf_files:
    try:
            # Read PDF file
            object = open(pdfs_path + pdf_file, 'rb') # rb means read binary
            reader = PyPDF2.PdfFileReader(object)
            
            # Get values from text boxes in the PDF
            #text_boxes_data = reader.getFormTextFields() # Deprecated, now getFields() works
            text_boxes_data = reader.getFields()

            # Get grades for the student
            student_grades = []
            for i in range(len(text_boxes_names)):
                grade = text_boxes_data.get(text_boxes_names[i], "").get("/V")
                student_grades.append(str(grade))
                
            # Add pdf file-name and the student's grades to the "line_data" array
            line = []
            line.append(pdf_file)
            for j in range (len(student_grades)):
                line.append(student_grades[j])
            line_data.append(line)
    except:
            # Add pdf file-name and the student's grades to the "line_data" array
            print("pdf file " + pdf_file + "could not be processed.")
            errors.append(pdf_file)
            
#------------------------Store grades in csv file-----------------------------#
with open (csv_file_name,'w',newline='') as f:
    writer = csv.writer(f)
    writer.writerow(headers)
    for i in range(0,len(line_data)):
        writer.writerow(line_data[i])
        
if len(errors) > 0:
    with open ("errors.csv",'w',newline='') as f:
        writer = csv.writer(f)
        for i in range(0,len(errors)):
            writer.writerow([errors[i]])
# End script
