#!/bin/bash

# Assignment suffix
ASSGTSUFFIX='example'

# Input parameters
inputfolder='script_input'
outputfolder='script_output'

timestamp=$(date "+%Y-%m-%d+%H%M%S")
outputbackupname="backup_output_$timestamp.tar"
inputbackupname="backup_input_$timestamp.tar"

# Create archive of output folder
mkdir -vp archives
tar -chvf $outputbackupname --exclude=old $outputfolder
mv -vf $outputbackupname archives/
tar -chvf $inputbackupname --exclude=old $inputfolder
mv -vf $inputbackupname archives/


# Import output of M9 and M6 into input of M10
rsync -avh --delete ../M9_collect_completed_grading/pdfs/ $inputfolder/pdfs/
rsync -avh --delete ../M6_match_peers/script_output/peers.csv $inputfolder/

# Clear output folder
rm -rvf $outputfolder/*

# Copy blank mark processing spreadsheet into output folder
cp -v grades_processing_template.ods $outputfolder/grades_processing_$ASSGTSUFFIX.ods


echo "end of script."
