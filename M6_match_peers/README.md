Script: match_peers.py
======================

## Description

This script takes a list of the students who received customized assignments and the pdf files sent by them. Then it assigns two peers (reviewers) to each student, except to students that did not submit any solution to their assignment.

The output of the script is a `csv` file containing the consolidated data.

The input files should be placed in the `script_input` folder which is in the same path as the script. The output files will
be saved in the `script_output` folder.

## Script input

1. `output_assignments.csv`: This file contains the data from the students including their custom ID. It is the output file from the script [assign_customizer.py](../M2_customize_assignments/assign_customizer.py) from [module 2](../M2_customize_assignments).

1. All the `pdf` files with the solutions submitted by the students, named as: `customID.pdf` (for example `12345678.pdf`).

## Script output

1. `peers.csv`: file with the assigned peers for each student.

1. renamed pdfs, ready to be sent for grading: `ID_A.pdf` → `ID_G.pdf`
