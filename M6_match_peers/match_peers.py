# -*- coding: utf-8 -*-
"""
Author: Germán Santa-Maria
Script: Match Peers
"""

"""-------------------------Hard coded input--------------------------------"""
# Input files paths
students_info_path = 'script_input/output_assignments.csv'
submission_pdfs_path = 'script_input/pdfs/' # path to folder containing input PDFs
# Output files paths
peers_csv_path = 'script_output/peers.csv'
"""-------------------------------------------------------------------------"""

# Import modules
import csv # For reading csv files
import pathlib # For working with local paths
import random  # For shuffling elements in arrays
from shutil import copyfile # To copy and rename the solution PDF files

# Initialize arrays to store data
names =              [] # Student first name
last_name =          [] # Student last name
emails =             [] # Student email address  
custom_ID_A =        [] # ID that the student uses to submit the solution
custom_ID_G =        [] # ID that the student uses to grade the peers
did_their_homework = [] # List of students that actually submitted a solution
ID_G_peer1 =         [] # List of peers (ID_G of first peer)
ID_G_peer2 =         [] # List of peers (ID_G of second peer)
random_peers1 =      [] # Temporary list of possible peers to assign
random_peers2 =      [] # Temporary list of possible peers to assign

# Store students data from csv file into variables
with open(students_info_path, newline='') as f:
    students_info_reader = csv.reader(f)
    next(students_info_reader) # Skip header [name, last name, email,... ]
    students_data = [row for row in students_info_reader]
    for row in students_data:
        names.append(row[0])
        last_name.append(row[1])
        emails.append(row[2])
        custom_ID_A.append(row[3])
        custom_ID_G.append(row[4])

# Check which students actually submitted their solution to their assignment by looking
# for a file in the "script_input" folder with the name "ID.pdf"
for i in range(0, len(custom_ID_A)):
    file_to_search = pathlib.Path(submission_pdfs_path+custom_ID_A[i]+'.pdf')        
    did_their_homework.append(file_to_search.exists())

########## Assign two peers to each student who submitted a solution ##########
###############################################################################

# Allocate size of arrays "ID_G_peer1" and "ID_G_peer2"
ID_G_peer1 = ['']*len(custom_ID_G) 
ID_G_peer2 = ['']*len(custom_ID_G) 

# Create two temporal arrays with the peers to be assigned 
for i in range(0,len(custom_ID_G)):
    # Only students who submitted solution can recieve a peer role 
    if did_their_homework[i]==True: 
        random_peers1.append(custom_ID_G[i] + 'A')
        random_peers2.append(custom_ID_G[i] + 'B')
copy_randp1 = random_peers1 # Save a copy of the array "random_peers1"
copy_randp2 = random_peers2 # Save a copy of the array "random_peers2"

# While loop to assign the peers
counter = 0 # Initialize counter for while loop
iterations = 0 
while counter<len(names) and iterations<10000: 
    # Shuffle "random_peers1" and "random_peers2" arrays
    random_peers1 = random.sample(random_peers1, len(random_peers1)) 
    random_peers2 = random.sample(random_peers2, len(random_peers2)) 
    
    # Make sure that peers are assigned only to students who submitted solution 
    if did_their_homework[counter]==False: 
        ID_G_peer1[counter] = 'None'
        ID_G_peer2[counter] = 'None'
        counter = counter + 1 # Update counter
        iterations = iterations + 1 # Update iterations counter
        continue # This jumps to the begining of the while loop
        
    # Make sure that the last student in the list does not end up without a 
    # peer, in the case that the last peer to be assigned is themselves.
    if len(random_peers1) == 1 and (custom_ID_G[counter] == random_peers1[0][0:-1] or 
            custom_ID_G[counter] == random_peers2[0][0:-1] or 
            random_peers1[0][0:-1] == random_peers2[0][0:-1]):
        random_peers1 = copy_randp1 # Resets "random_peers1" array
        random_peers2 = copy_randp2 # Resets "random_peers2" array
        counter = 0 # This resets counter
        iterations = iterations + 1 # Update iterations counter
        continue
    
    # If student==peer or peer1==peer2 repeat iteration
    if (custom_ID_G[counter] == random_peers1[0][0:-1] or 
            custom_ID_G[counter] == random_peers2[0][0:-1] or 
            random_peers1[0][0:-1] == random_peers2[0][0:-1]): 
        iterations = iterations + 1 # Update iterations counter
        continue # "counter" is not updated, iteration will be repeated
    
    # If no conflicts are found assign peer1 and peer2
    ID_G_peer1[counter] = random_peers1[0] # Assign first peer
    random_peers1.pop(0) # Remove peer from "random_peers1"
    ID_G_peer2[counter] = random_peers2[0] # Assign second peer
    random_peers2.pop(0) # Remove peer from "random_peers2" 
    counter = counter + 1 # Update counter
    iterations = iterations + 1 # Update iterations counter
    
###############################################################################
###############################################################################

# Store the students and their respective peers data in a csv file
with open (peers_csv_path,'w',newline='') as f:
    writer = csv.writer(f)
    writer.writerow(['name','lastname','email','custom_ID_A','custom_ID_G',
                     'submitted','peer1_ID','peer2_ID'])
    for i in range(0,len(names)):
        writer.writerow([names[i],last_name[i],emails[i],custom_ID_A[i],
              custom_ID_G[i],str(did_their_homework[i]),ID_G_peer1[i],
              ID_G_peer2[i]])

# Copy and rename the submission PDF files 
# (custom_ID_A  -> customID_G)
for i in range(0, len(custom_ID_A)):
    if did_their_homework[i]==True:
        copyfile(submission_pdfs_path + custom_ID_A[i] + ".pdf", 
            "script_output/pdfs/" + custom_ID_G[i] + ".pdf")
# End of script
