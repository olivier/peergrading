#!/bin/bash

# Input parameters
inputfolder='script_input'
outputfolder='script_output'

timestamp=$(date "+%Y-%m-%d+%H%M%S")
outputbackupname="backup_output_$timestamp.tar"
inputbackupname="backup_input_$timestamp.tar"

# Create archive of output folder
mkdir -vp archives
tar -chvf $outputbackupname --exclude=old $outputfolder
mv -vf $outputbackupname archives/
tar -chvf $inputbackupname --exclude=old $inputfolder
mv -vf $inputbackupname archives/


# Clear output folder
rm -rvf $outputfolder/*

# Create subdir "pdfs" in output folder
mkdir -pv $outputfolder/pdfs


# Import output of M5 into input of M6
rsync -avh --delete ../M5_collect_submissions/pdfs/ $inputfolder/pdfs/
# Import list of assignments (output of M2) into input of M6
rsync -avh ../M2_customize_assignments/script_output/output_assignments.csv $inputfolder/


echo "end of script."
