# -*- coding: utf-8 -*-
"""
Author: Germán Santa-Maria
Script: Hash Function

 Description:
 This script takes an email address (for example: student1@example.com)
 and uses a cryptographic hash function and that generates two nine-digit 
 anonymous IDs to identify the student assignment and grade respectively.
 Based on the input email address, the script also generates an array
 with six random numbers that take values from 0 to 1.
 
"""

def hash_function(student_email):
   import hashlib
   salt = "insert_your_salt_here" # Salt for the hash function
   hash_input = salt + str(student_email) # Input for hash function
   hf = hashlib.md5(hash_input.encode('utf-8')).hexdigest() # Digest hash
   hf = str(int(hf,16)) # Convert from hexadecimal to integer string
   
   # Generate Custom ID #1 for the student assignment
   custom_ID_A = hf[1:9] 
   custom_ID_A = "A"+str(custom_ID_A) 
   
   # Generate Custom ID #2 for the student grade
   custom_ID_G = hf[10:18]
   custom_ID_G = "G"+str(custom_ID_G) 
   
   rand_n = [hf[9:12],hf[12:15],hf[15:18],hf[18:21],hf[21:24],hf[24:27]]
   rand_n = list(map(int, rand_n)) # Random numbers from list to number
   
   for i in range(len(rand_n)): # Turn random numbers into values from 0 to 1
       rand_n[i] = rand_n[i]*0.001
      
   return [custom_ID_A, custom_ID_G, rand_n]

# End of script
