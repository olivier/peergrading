#!/bin/bash

# Input parameters
inputfolder='script_input'
outputfolder='script_output'

timestamp=$(date "+%Y-%m-%d+%H%M%S")
outputbackupname="backup_output_$timestamp.tar"
inputbackupname="backup_input_$timestamp.tar"

# Create archive of output folder
mkdir -vp archives
tar -chvf $outputbackupname --exclude=old $outputfolder
mv -vf $outputbackupname archives/
tar -chvf $inputbackupname --exclude=old $inputfolder
mv -vf $inputbackupname archives/


# Clear output folder
rm -rvf $outputfolder/*

# Import any PNG from input root folder into output
rsync -avh --delete --delete-excluded --prune-empty-dirs --include="*/" --exclude="*/*" --include="*.png" --exclude="*" $inputfolder/ $outputfolder/

echo "end of script."
