#!/bin/bash

# Assignment suffix
ASSGTSUFFIX='example'

# Input parameters
inputfolder='script_input'
outputfolder='script_output'

# Prepare output folder
mkdir -v $outputfolder/done

# Copy input into the output folder, change directory to output folder
cp -vrf $inputfolder/* $outputfolder/
cd $outputfolder

# Copy grading form to work folder (it’s the same file for all )
cp -v form/form_$ASSGTSUFFIX.pdf ./form.pdf

# Pick up all the "A" files in input subfolder one by one
for submissionpdf in submissions/*.pdf; do

	# Extract IDG code from submission pdf file name
	IDG=$(echo "$submissionpdf" | sed -r 's/submissions\///' | sed -r 's/\.pdf//')
	
	# Construct IDGA and IDGB codes out of IDG code
	APPENDA='A'
	APPENDB='B'
	IDGA=$IDG$APPENDA
	IDGB=$IDG$APPENDB

	echo "#"
	echo "#"
	echo "building assignment PDF for $IDG …"

	mv -v solutions/solution*_$IDG.pdf ./solution.pdf
	mv -v submissions/$IDG.pdf ./submission.pdf

	# Concatenate all three PDFs that we have together now
	# Note that pdftk is not available by default on Ubuntu 18.04,
	# see https://askubuntu.com/a/1046476 for workaround install
	pdftk A=solution.pdf B=submission.pdf C=form.pdf cat A B C output done/$IDGA.pdf
	
	# Create clone of PDF file with B code
	cp -v done/$IDGA.pdf done/$IDGB.pdf
	
	done


echo "#"
echo "#"
echo "#"
echo "done building PDFs"

# Cleanup: remove temporary folders
rm -rvf form solutions submissions form.pdf solution.pdf submission.pdf

cd ../

echo "end of script."
