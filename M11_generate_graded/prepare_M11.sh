#!/bin/bash

# Input parameters
inputfolder='script_input'
outputfolder='script_output'

timestamp=$(date "+%Y-%m-%d+%H%M%S")
outputbackupname="backup_output_$timestamp.tar"
inputbackupname="backup_input_$timestamp.tar"

# Create archive of output folder
mkdir -vp archives
tar -chvf $outputbackupname --exclude=old $outputfolder
mv -vf $outputbackupname archives/
tar -chvf $inputbackupname --exclude=old $inputfolder
mv -vf $inputbackupname archives/


# Import output of M3 into input of M11 (only those files matching solution_graded* )
rsync -avh --delete --delete-excluded --prune-empty-dirs --include="*/" --include="solution_graded*" --exclude="*" ../M3_generate_assignments/script_output/ $inputfolder/solutions/
# Import output of M3 and M5 into input of M11
rsync -avh --delete ../M5_collect_submissions/pdfs/ $inputfolder/submissions/


# Clear output folder
rm -rvf $outputfolder/*


echo "end of script."
