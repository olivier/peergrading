#!/bin/bash

# Assignment suffix
ASSGTSUFFIX='example'

# Input parameters
inputfolder='script_input'
outputfolder='script_output'

# Prepare output folder
mkdir -pv $outputfolder/done

# Copy input into the output folder, change directory to output folder
cp -vrf $inputfolder/* $outputfolder/
cd $outputfolder

mkdir "done"

# Pick up all the files in input subfolder one by one
for submissionpdf in submissions/*.pdf; do

	# Extract IDA code from submission pdf file name
	IDA=$(echo "$submissionpdf" | sed -r 's/submissions\///' | sed -r 's/\.pdf//')

	echo "#"
	echo "#"
	echo "building assignment PDF for $IDA …"

	mv -v solutions/solution_graded_${ASSGTSUFFIX}_$IDA.pdf ./solution.pdf
	mv -v submissions/$IDA.pdf ./submission.pdf

	# Concatenate all three PDFs that we have together now
	# Note that pdftk is not available by default on Ubuntu 18.04,
	# see https://askubuntu.com/a/1046476 for workaround install
	pdftk A=solution.pdf B=submission.pdf cat A B output done/Solution_submission_$IDA.pdf
	
	done


echo "#"
echo "#"
echo "#"
echo "done building PDFs"

# Cleanup: remove temporary folders
rm -rvf form solutions submissions solution.pdf submission.pdf

cd ../

echo "end of script."
