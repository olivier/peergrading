#!/bin/bash

# Input parameters
inputfolder='script_input'
outputfolder='script_output'

timestamp=$(date "+%Y-%m-%d+%H%M%S")
outputbackupname="backup_output_$timestamp.tar"
inputbackupname="backup_input_$timestamp.tar"

# Create archive of output folder
mkdir -vp archives
tar -chvf $outputbackupname --exclude=old $outputfolder
mv -vf $outputbackupname archives/
tar -chvf $inputbackupname --exclude=old $inputfolder
mv -vf $inputbackupname archives/


# Import grade record (output of M10) into input of M12
rsync -avh --delete ../M10_process_grades/script_output/grades_to_send.csv $inputfolder/
# Clear empty lines in grades_to_send.csv
sed -i '/"","","","","","","","","","","","","","","","","","","","","","","",""/d' $inputfolder/grades_to_send.csv

# Import output of M11 into input of M12
rsync -avh --delete ../M11_generate_graded/script_output/done/ $inputfolder/pdfs/


# Clear output folder
rm -rvf $outputfolder/*


echo "end of script."
