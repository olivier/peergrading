#!/bin/bash

# Input parameters
inputfolder='script_input'
outputfolder='script_output'

# Copy every tex file in the input folder into the output folder
cp -vf $inputfolder/*.tex $outputfolder/
cp -vf $inputfolder/*.png $outputfolder/

# Pick up all the files in input folder one by one
cd $outputfolder
for assignmentfile in assignment*.tex; do

	echo "#"
	echo "#"
	echo "building assignment PDF for $assignmentfile …"
	
	# Empty up the tmp subfolder, if it exists
	rm -rvf tmp/*
	# Make a tmp folder
	mkdir -pv tmp

	# Compile the actual PDF file:
	pdflatex $assignmentfile
	pdflatex $assignmentfile

	# Cleanup after pdflatex: move temporary files to tmp subfolder
	mv -vf *.aux *.bbl *.bcf *.blg *.ilg *.log *.nav *.nlo *.nls *.run.xml *.snm *.toc *.out *.fls *.fdb_latexmk *.spl *-blx.bib *-eps-converted-to.pdf *bib.bak tmp/
	
	done

for solutionfile in solution*.tex; do

	echo "#"
	echo "#"
	echo "building solution PDF for $solutionfile …"
	
	# Empty up the tmp subfolder, if it exists
	rm -rvf tmp/*
	# Make a tmp folder
	mkdir -pv tmp

	# Compile the actual PDF file:
	pdflatex $solutionfile
	pdflatex $solutionfile

	# Cleanup after pdflatex: move temporary files to tmp subfolder
	mv -vf *.aux *.bbl *.bcf *.blg *.ilg *.log *.nav *.nlo *.nls *.run.xml *.snm *.toc *.out *.fls *.fdb_latexmk *.spl *-blx.bib *-eps-converted-to.pdf *bib.bak tmp/
	
	done

cd ../

# Remove tex and tmp files/folders from output folder
rm -vf $outputfolder/*.tex
rm -vf $outputfolder/*.png
rm -vrf $outputfolder/tmp

echo "end of script."
