Script: create_pdfs.sh
======================

## Description

This script takes a series of `tex` files containing each a series of LaTeX variables with custom values, and produces, for each of those, a `pdf` file. In total, for each participant, there should be one assignment, one solution to be later sent to peers, and one solution to be later sent back to the participant.

## Dependencies

The script is run in a bash shell on a Ubuntu system. It makes use of the `pdflatex` compiler. The example `tex` file uses stylesheets from the [sensible styles](https://framagit.org/olivier/sensible-styles) library.
