#!/bin/bash

# Input parameters
inputfolder='script_input'
outputfolder='script_output'

timestamp=$(date "+%Y-%m-%d+%H%M%S")
outputbackupname="backup_output_$timestamp.tar"
inputbackupname="backup_input_$timestamp.tar"

# Create archive of output folder
mkdir -vp archives
tar -chvf $outputbackupname --exclude=old $outputfolder
mv -vf $outputbackupname archives/
tar -chvf $inputbackupname --exclude=old $inputfolder
mv -vf $inputbackupname archives/


# Import output of M3 into input of M4
rsync -avh --delete ../M3_generate_assignments/script_output/ $inputfolder/
rsync -avh ../M2_customize_assignments/script_output/output_assignments.csv $inputfolder/

# Clear output folder
rm -rvf $outputfolder/*


echo "end of script."
