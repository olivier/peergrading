Script: email_assignments.py
============================

## Description

This script sends personalized emails with attachments.

## Script input

1. `output_assignments.csv`: This file contains the data from the students including their custom ID. It is the output file from the script [assign_customizer.py](../M2_customize_assignments/assign_customizer.py) from [module 2](../M2_customize_assignments).
    
1. All the pdf files with the assignments for the students, for example: `A12345678.pdf`. The contents of this folder are the output from the script [create_pdfs.sh](../M3_generate_assignments/create_pdfs.sh) from [module 3](../M3_generate_assignments).
        
1. Email settings are hard-coded in this script, except for the password. Sender email password is requested in a message box when the script is runing.

## Script output

1. One personalized email per student, at a controlled rate in order not to exceed local network limitations;

1. `sent_emails.csv` : file with the log of the sent emails
