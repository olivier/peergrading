# -*- coding: utf-8 -*-
"""
Author: Germán Santa-Maria
Script: Email Assignments
"""

###########################################################################
# Parameters specific to assignment exercise

# Number of assignment
assignment_suffix = "_example"
###########################################################################

"""-------------------------Hard coded input--------------------------------"""
# Input file paths
input_csv_path = 'script_input/output_assignments.csv'
assignments_pdfs_path = 'script_input/'

# Output file paths
output_csv_path = 'script_output/sent_emails.csv'

# (Hard-coded email settings)
host="myhost.example.com"
port = 587
sender_email = "sender@example.com"
cc = "cc_address@example.com"
sender_username = "username"
password = input("Type your password and press enter:")

# Delay time between sending two emails, in seconds
# For example, set 240 seconds if you are limited to less than 30 emails/hour
# and 40 seconds if the limit is less than 30 emails/10 min
delay_time = 2 # just 2 seconds, low enough to be fast, high enough to see what is happening
"""-------------------------------------------------------------------------"""

# Import email sender function
import csv # For reading cvs files
import pathlib # For working with local paths
import smtplib, ssl # For email
import time # For timer 
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

# Initialize arrays to store data
first_name =     [] # Student first name
last_name =      [] # Student last name
emails =         [] # Student email address 
custom_ID_A =    [] # Custom ID of the assignment for the student
custom_ID_G =    [] # Custom ID of the grading file for the student
buddy_name =     [] # First name of buddy
buddy_email =    [] # Email of buddy
buddytwo_name =  []
buddytwo_email = []
attachments =    [] # Name of the attachment for each student
email_bodies =   [] # Body of the email for each student
text_emails =    [] # Array with the complete email text for each student
email_log =      [] # Array with the log of successfully sent emails

# Store students data from csv file into variables
with open(input_csv_path, newline='') as f:
    student_info_reader = csv.reader(f)
    next(student_info_reader) # Skip header [name, last name, email, ...]
    student_data = [row for row in student_info_reader]
    for row in student_data:
        first_name.append(row[0])
        last_name.append(row[1])
        emails.append(row[2])
        custom_ID_A.append(row[3])
        custom_ID_G.append(row[4])
        buddy_name.append(row[5])
        buddy_email.append(row[6])
        buddytwo_name.append(row[7])
        buddytwo_email.append(row[8])

for ID in custom_ID_A:
    attachment_name = "assignment"+assignment_suffix+"_"+ID+".pdf"
    attachments.append(attachment_name) 

# Make sure that all the attachments are in the script_input folder by looking 
# for a file with the correct file name
for i in range(0, len(custom_ID_A)):
    file_to_search = pathlib.Path(assignments_pdfs_path+attachments[i])        
    if file_to_search.exists() != True: 
        raise Exception('The file ' + 
                        assignments_pdfs_path+attachments[i]+
                        ' was not found, no emails were sent')


###########################################################################
# Content specific to assignment exercise

email_subject = "Your coursework assignment #1"

standard_body_onebuddy = """
Dear XXstudent_first_nameXX,

I have the pleasure of handing you (attached) your first coursework assignment in our course. I hope you will participate in the program, and have fun doing so! The coursework program has a total of three assignments, and is managed entirely by e-mail. Below, I explain how the program works and what to expect.

...
(insert email body here)
...


Buddy Program
=============

For this assignment, we are introducing you to a work buddy! Your buddy for this assignment is XXbuddy_first_nameXX, and his or her email is XXbuddy_emailXX .

We paired up buddies to give everyone a chance to work with somebody different than usual. Your grade and your buddy’s grade are completely unrelated, and you still receive different assignments. You may work together if you wish, sharing your findings and reviewing one another’s work. Or, you may quietly ignore one another. It is completely up to you. Still, we hope you will take the chance, and ping them for a Zoom talk or something.


Grading
=======

Your assignment will be graded anonymously by two fellow students. In turn, next week, you will be asked to grade two assignments from other students. To do this, you will be provided with a complete answer sheet and a marking template. You are required to grade your peers rigorously and thoroughly in order to receive a grade yourself.


Format of your answer
=====================

Please send one PDF file containing your complete answer. The PDF file should be named:
XXsubmissionfilenameXX
Send your file to this email address as an attachment (the title and content of the email do not matter; they will not be read).

Your answer will be graded anonymously, and so it must not contain *any* personally-identifying information: no name, no pseudonym, no matriculation number, no code number of any kind. Non-anonymous answers will be automatically discarded.

You can write your answer as you like (i.e. by hand, or by typing it up), and create a PDF with your preferred tools. It only matters that your answer arrives as a clearly-legible PDF document.


Conclusion
==========

This is a lot of explanations, but in principle, this should be fun, and the coursework is not very hard. If you have any question about how this program works, you can contact me at (redacted). I look forward to receiving your answer!

With best regards,

(Author)

"""
###########################################################################

# Email for student with two buddies
standard_body_twobuddies = """
Dear XXstudent_first_nameXX,

I have the pleasure of handing you (attached) your first coursework assignment in our course. I hope you will participate in the program, and have fun doing so! The coursework program has a total of three assignments, and is managed entirely by e-mail. Below, I explain how the program works and what to expect.

...
(insert email body here)
...


Buddy Program
=============

For this last assignment, we are introducing you to a work buddy! Because there are an odd number of students in this program, you are matched to two buddies this time.

Your first buddy for this assignment is XXbuddy_first_nameXX, and his or her email is XXbuddy_emailXX. Your second buddy is XXbuddytwo_first_nameXX, and his or her email is XXbuddytwo_emailXX .

We paired up buddies to give everyone a chance to work with somebody different than usual. Your grade and your buddies’ grades are completely unrelated, and you still receive different assignments. You may work together if you wish, sharing your findings and reviewing one another’s work. Or, you may quietly ignore one another. It is completely up to you. Still, we hope you will take the chance, and ping them for a Zoom talk or something.


Grading
=======

Your assignment will be graded anonymously by two fellow students. In turn, next week, you will be asked to grade two assignments from other students. To do this, you will be provided with a complete answer sheet and a marking template. You are required to grade your peers rigorously and thoroughly in order to receive a grade yourself.


Format of your answer
=====================

Please send one PDF file containing your complete answer. The PDF file should be named:
XXsubmissionfilenameXX
Send your file to this email address as an attachment (the title and content of the email do not matter; they will not be read).

Your answer will be graded anonymously, and so it must not contain *any* personally-identifying information: no name, no pseudonym, no matriculation number, no code number of any kind. Non-anonymous answers will be automatically discarded.

You can write your answer as you like (i.e. by hand, or by typing it up), and create a PDF with your preferred tools. It only matters that your answer arrives as a clearly-legible PDF document.


Conclusion
==========

This is a lot of explanations, but in principle, this should be fun, and the coursework is not very hard. If you have any question about how this program works, you can contact me at (redacted). I look forward to receiving your answer!

With best regards,

(Author)

"""

###########################################################################

for i in range(0, len(first_name)):
    if len(buddytwo_name[i]) > 1:  # check if a 2nd buddy exist
        temp_body = standard_body_twobuddies.replace("XXstudent_first_nameXX", first_name[i])
        temp_body = temp_body.replace("XXbuddy_first_nameXX", buddy_name[i])
        temp_body = temp_body.replace("XXbuddy_emailXX", buddy_email[i])
        temp_body = temp_body.replace("XXbuddytwo_first_nameXX", buddytwo_name[i])
        temp_body = temp_body.replace("XXbuddytwo_emailXX", buddytwo_email[i])
        submission_file_name = custom_ID_A[i] + ".pdf"
        temp_body = temp_body.replace("XXsubmissionfilenameXX", submission_file_name)
        email_bodies.append(temp_body)
    else:
        temp_body = standard_body_onebuddy.replace("XXstudent_first_nameXX", first_name[i])
        temp_body = temp_body.replace("XXbuddy_first_nameXX", buddy_name[i])
        temp_body = temp_body.replace("XXbuddy_emailXX", buddy_email[i])
        submission_file_name = custom_ID_A[i] + ".pdf"
        temp_body = temp_body.replace("XXsubmissionfilenameXX", submission_file_name)
        email_bodies.append(temp_body)

# Compose email text for each student
for i in range(0, len(first_name)):
    # Reciever email
    receiver_email = emails[i]
    
    # Email subject
    subject = email_subject
    
    # Email body
    body = email_bodies[i]
    
    # Attachment
    filename = attachments[i]
    
    # Set headers
    message = MIMEMultipart()
    message["From"] = sender_email
    message["To"] = receiver_email
    message["Subject"] = subject
    message["Cc"] = cc
    
    # Add body to email
    message.attach(MIMEText(body, "plain"))
    
    # Add attachment
    attachment_path = assignments_pdfs_path+filename # Attachment folder path 
    
    # Open PDF file in binary mode
    with open(attachment_path, "rb") as attachment:
        # Add file as application/octet-stream
        part = MIMEBase("application", "octet-stream")
        part.set_payload(attachment.read())

    # Encode file in ASCII characters to send by email    
    encoders.encode_base64(part)
    
    # Add header as key/value pair to attachment part
    part.add_header("Content-Disposition",
                    "attachment", filename=filename)

    # Add attachment to message and convert message to string
    message.attach(part)
    text = message.as_string()
    text_emails.append(text)
"""-------------------------------------------------------------------------"""

# Send an email to each student with the corresponding attachment
"""------------------------------Send emails--------------------------------"""
# Create a secure SSL context
context = ssl.create_default_context()

# Check if the login data is correct

# Commented out for bulk sending of emails
# try :
#     server = smtplib.SMTP(host = host, port = port)
#     server.starttls(context = context) #Encryption
#     server.login(sender_username, password)
#     print('Login to: '+ sender_email + ' was successful ')
# except Exception as e:
#     # Print error message
#     print('Login to: '+ sender_email +
#           ' was not successful, no emails were sent ')
#     raise Exception(e)
# server.close() # Logout

# Send email to each student
for i in range(0, len(first_name)):
    try :
        # Reciever email
        receiver_email = emails[i]
        # Email content
        text = text_emails[i]
        # Logging into email server
        server = smtplib.SMTP(host = host, port = port)
        server.starttls(context = context) #Encryption
        # server.login(sender_username, password)  # Commented out for bulk sending of emails
        # Send email
        server.sendmail(sender_email, [receiver_email, cc], text)
        # Logout
        server.close() # Logout
        # Print email log
        local_time = time.time()
        time_sent = time.ctime(local_time) 
        log_m = 'Email was sent to: ' + receiver_email + ' time: ' + time_sent
        print(log_m)
    except Exception as e:
        # Print error
        log_m = 'Error while trying to send email to: ' + receiver_email 
        print(log_m)
        print(e)
        log_m = 'Email could not be sent, error: ' + str(e)
    
    # Save log
    email_log.append(log_m)
    
    # Wait before sending the next email
    time.sleep(delay_time)  

# Store email log in a csv file
with open (output_csv_path,'w',newline='') as f:
    writer = csv.writer(f)
    writer.writerow(['name','lastname','email','custom_ID_A','custom_ID_G',
                     'log'])
    for i in range(0,len(first_name)):
        writer.writerow([first_name[i],last_name[i],emails[i],custom_ID_A[i],
                         custom_ID_G[i], email_log[i]])
    
"""-------------------------------------------------------------------------"""

# End of the script
