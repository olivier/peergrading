Peer-graded student exercises
=============================

A collection of scripts (in Python, bash, LaTeX) to build and assign peer-graded individualized assignments for a large number of students. Each student is to:
1. register by email;
1. receive their own problem, built with unique input parameters;
1. submit their answer by email;
1. receive two anonymized answers from peer students, each with the corresponding solution;
1. grade those two answers, and submit them by email;
1. receive their own peer-assigned grade and the solution to their own problem.

This tool is used to operate a program of peer-graded coursework in the [Fluid Mechanics course](https://fluidmech.ninja/) of the Chemical & Energy Engineering program in the [Otto von Guericke University Magdeburg](https://www.ovgu.de), Germany. Part of the development funded by a grant of the University’s “Innovation in Studien und Lehre” program.

The tool is described in a peer-reviewed journal publication: O. Cleynen, G. Santa-Maria, M. Magdowski, and D. Thévenin. “Peer-graded individualized student homework in a single-instructor undergraduate engineering course”. In: *Research in Learning Technology* 28 (2020). [doi: 10.25304/rlt.v28.2339](https://doi.org/10.25304/rlt.v28.2339) .

## Authors

Germán Santa-Maria, Jochen König, [Olivier Cleynen](https://ariadacapo.net/), with the kind financial support of the [LSS laboratory](http://www.lss.ovgu.de) of the university, and of the university itself. Inspiration and guidance were gratefully received from [Mathias Magdowski](http://www.imt.ovgu.de/Das+Institut/Mitarbeiterinnen+und+Mitarbeiter/visitenkarten/magdowski__mathias-kat-lehrveranstaltungen-p-71.html) (see [[1]](https://www.slideshare.net/MathiasMagdowski/personalisierbare-aufgaben-und-anonymer-peerreview/), [[2]](https://www.researchgate.net/publication/330243310_Personalisierbare_Aufgaben_und_anonymer_Peer-Review)).

## License

The code in this repository is published under the [GNU GPL v3](href="https://www.gnu.org/licenses/gpl.html) license (see file [LICENSE](LICENSE) for full text). Please cite the *Research in Learning Technology* article above if this tool is useful to you.

## Structure

A total of 12 modules (9 of which being locally-run scripts) are needed to run the project. 

[ ] Module 1: Collect participant information
(through email)

[x] [Module 2](M2_customize_assignments): Generate custom variables for assignments
(in Python)

[x] [Module 3](M3_generate_assignments): Generate assignments and solutions
(in bash + LaTeX)

[x] [Module 4](M4_send_assignments): Send assignments to students
(in Python)

[ ] Module 5: Receive submissions from students
(through email)

[x] [Module 6](M6_match_peers): Match peers
(in Python)

[x] [Module 7](M7_generate_grading): Generate grading assignments
(in bash, with pdftk)

[x] [Module 8](M8_send_grading): Send peer grading assignments
(in Python)

[ ] Module 9: Collect peer-assigned grades
(through email)

[x] [Module 10](M10_process_grades): Process grades
(in Python)

[x] [Module 11](M11_generate_graded): Generate graded PDFs
(in bash, with pdftk)

[x] [Module 12](M12_email_graded): Send results to students
(in Python)
